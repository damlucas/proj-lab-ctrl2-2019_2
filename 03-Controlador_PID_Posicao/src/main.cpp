
/*=================================================================================================
*  Projeto:   Controlador PID para posição de cabeçote de impressora
*
*  Objetivo:  Aplicar técnicas de sistemas controle para garantir um 
*             controlador PID devidamente sintonizado
*
*  Obs.:      Este código faz uso do projeto Obter Planta (FTPR) como base
*             aproveitando as declarações de pinos, constantes e variáveis globais
*
*  Autor:     Lucas Damião  - Estudante de Eng. Elétrica - UNIVASF
*
*  Criado:    15/12/19  Mod.: dd/mm/aa
*
*================================================================================================*/

/* --- Bibliotecas --- */
#include <Arduino.h>
#include <sPID.h>
#include <LiquidCrystal_I2C.h>

/* --- Definições --- */
#define pFC       2             // Conexão do sensor de fim de curso

#define clk_enc   3             // Conexão do encoder
#define data_enc  4

#define p1_motor  9             // Controle do motor
#define p2_motor  10

#define pot       A2

#define conversor 0.17         // Fator de conversão pulsos => cm
#define pos_min   0.0	  				// Posição mínima do cabeçote em cm (ajustável)
#define pos_max   280.0	  			// Posição máxima do cabeçote em cm (ajustável)

#define pwm_min 160             // Valores mínimo e máximo de PWM para aplicar ao motor
#define pwm_max 255

#define pos_err 0.35

#define tm      800             // Tempo de espera para rotinas de inicialização

#define kp    0.0837            // Constantes para o controlador
#define ki    0.0105
#define kd    0.00669


/* --- Macros --- */
#define   set_bit(reg,bit)  (reg|=(1<<bit))
#define   clr_bit(reg,bit)  (reg&=~(1<<bit))
#define   tst_bit(reg,bit)  (reg&(1<<bit))

/* --- Declaração de Funções Adicionais --- */
void bordaA(void);                    // Função a ser chamada pela interrupção do encXa
void fimdeCurso(void);					      // Função a ser chamada pela interrupção do pino fcX
void zerarPos(void);						      // Função para zerar a posição do eixo
byte condcVeloc(float _veloc, float _error);
void disp(void);

/* --- Objetos --- */
sPID myPID(kp, ki, kd, conversor);
LiquidCrystal_I2C lcd(0x3F, 20, 4);

/* --- Variáveis Globais --- */
volatile int encoder = 0;					// Contador do sinal do encoder linear
float posicao = 0.0;
unsigned long ti;
float t;
float set_point = 0.0;
byte pwm = 0;

/* --- Funções Obrigatórias --- */
void setup()
{
  Serial.begin(500000);
  lcd.begin();

  lcd.setCursor(0,0);
  lcd.print("Lab de Controle 2");
  lcd.setCursor(0,1);
  lcd.print("Controle de Posicao");

  //Config pinos
  lcd.setCursor(0,2);
  lcd.print("Config pinos...   ");
  pinMode( pFC, INPUT_PULLUP);
  pinMode(clk_enc, INPUT_PULLUP);
  pinMode(data_enc, INPUT_PULLUP);
  
  set_bit(DDRB, PB1);
  set_bit(DDRB, PB2);
  delay(tm/2);
  lcd.print("OK");
  delay(tm/2);


  //Config interrupções
  lcd.setCursor(0,3);
  lcd.print("Config interrup...");
  attachInterrupt(digitalPinToInterrupt(pFC), fimdeCurso, CHANGE);
  attachInterrupt(digitalPinToInterrupt(clk_enc), bordaA, RISING);
  delay(tm/2);
  lcd.print("OK");
  delay(tm);

  //Coloca cabeçote na posição inicial
  lcd.setCursor(0,2);
  lcd.print("Posicao inicial...  ");
  lcd.setCursor(0,3);
  lcd.print("                    ");
  zerarPos();
  lcd.setCursor(18,2);
  delay(tm/2);
  lcd.print("OK");
  delay(tm);

  lcd.setCursor(0,2);
  lcd.print("Pos. Atual:         ");
  lcd.setCursor(0,3);
  lcd.print("Pos. Ref: ");

}

void loop()
{
  set_point = pos_max * (analogRead(pot) / 1023.0);
  myPID.newSetPoint(set_point);
  
  posicao = conversor * encoder;
  float res_pid = myPID.process(posicao);

  float err = myPID.error;

  pwm = condcVeloc(res_pid, err);

  if(res_pid > 0)
  {
    digitalWrite(p2_motor, LOW);
    analogWrite(p1_motor,  pwm);
  }
  else if(res_pid <0)
  {
    digitalWrite(p1_motor, LOW);
    analogWrite(p2_motor,  pwm);
  }
  else
  {
    digitalWrite(p1_motor, LOW);
    digitalWrite(p2_motor, LOW);
  }

  disp();

  Serial.print(String(posicao) + "\t" + String(set_point) + "\t");
  Serial.println(res_pid);

}

/* --- Escopo das Funções Adicionais --- */
void bordaA(void)           // Função chamada pela interrupção em 'clk_enc'
{
  if (tst_bit(PIND, data_enc))
    encoder--;
  else
    encoder++;
}

void fimdeCurso(void)       // Função a ser chamada pela interrupção no pino 'pFC'
{
  digitalWrite(p1_motor,LOW);
  digitalWrite(p2_motor,LOW);
  encoder = 0;
}

void zerarPos(void)
{
  digitalWrite(p2_motor,LOW);
  analogWrite(p1_motor, pwm_max);			// Move o cabeçote nos dois sentidos para garantir o 
                                    //   acionamento do sensor de fim de curso
  delay(tm);

  digitalWrite(p1_motor,LOW);
  analogWrite(p2_motor, pwm_min);

  while (tst_bit(PIND,pFC));		// Aguarda o acionamento do sensor de fim de curso

  digitalWrite(p1_motor,HIGH);
  digitalWrite(p2_motor,HIGH);
  delay(200);

  digitalWrite(p1_motor,LOW);
  digitalWrite(p2_motor,LOW);

  encoder = 0;
}

byte condcVeloc(float _veloc, float _error)
{
  byte veloc = 0;

  if(abs(_veloc) < 1)
    veloc = 0;
  else
    veloc = byte(pwm_min + abs(_veloc));

  return veloc;
}

void disp(void)
{
  lcd.setCursor(12,2);
  if(posicao < 100)
  {
    if(posicao < 10)
      lcd.print("  ");
    else
      lcd.print(" ");
  }
  lcd.print(posicao);
  lcd.print(" ");

  lcd.setCursor(12,3);
  if(set_point < 100)
  {
    if(set_point < 10)
      lcd.print("  ");
    else
      lcd.print(" ");
  }
  lcd.print(set_point);
  lcd.print(" ");
}