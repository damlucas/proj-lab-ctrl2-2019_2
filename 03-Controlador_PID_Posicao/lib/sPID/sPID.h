/*  This is a simple implemantation of PID controller for basic process   */


#ifndef _sPID_h_
#define _sPID_h_

#endif

class sPID
{	
    public:  
        float  error, delta_e, kP, kI, kD;

        sPID(float _kP, float _kI, float _kD, float _delta_e);
        void constConfig(float _kP, float _kI, float _kD);
        void newSetPoint(double _setPoint);
        float process(float _sample);

    private:
        float   setPoint,
                sample,
                lastError,
                P, I, D;
        long    lastProcess;

};