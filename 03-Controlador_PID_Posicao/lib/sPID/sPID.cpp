/*  This is a simple implemantation of PID controller for basic process   */

#if (ARDUINO >= 100)
  #include "Arduino.h"
#else
  #if defined(__AVR__)
    #include <avr/io.h>
  #endif
  #include "WProgram.h"
#endif

#include "sPID.h"


sPID::sPID(float _kP, float _kI, float _kD, float _delta_e)
{
    delta_e = _delta_e;
    kP = _kP;
    kI = _kI;
    kD = _kD;
}

void sPID::constConfig(float _kP, float _kI, float _kD)
{
    kP = _kP;
    kI = _kI;
    kD = _kD;
}

void sPID::newSetPoint(double _setPoint)
{
    setPoint = _setPoint;
}

float sPID::process(float _sample)
{

    // Processing PID Implementation
    error = setPoint - _sample;
    float deltaTime = (millis() - lastProcess) / 1000.0;
    lastProcess = millis();

    //P
    P = error * kP;

    //I
    I = I + kI * (error * deltaTime);

    //D
    D = kD * ( (error - lastError) / deltaTime);
    lastError = error;

    // Return Sum
    return (P + I + D);
}
