
#if (ARDUINO >= 100)
  #include "Arduino.h"
#else
  #if defined(__AVR__)
    #include <avr/io.h>
  #endif
  #include "WProgram.h"
#endif

#include "Eixo.h"

/* --- Macros --- */
#define   set_bit(reg,bit)  (reg |= (1<<bit))
#define   clr_bit(reg,bit)  (reg &= ~(1<<bit))
#define   tst_bit(reg,bit)  (reg &  (1<<bit))

Eixo::Eixo(uint8_t _data_enc, bool _increment)
{
    data_enc = _data_enc;
    increment_order = _increment;
}

Eixo::Eixo(uint8_t _data_enc)
{
    data_enc = _data_enc;
}

void Eixo::begin(void)
{
    // end_of_course config === INPUT_PULLUP
    clr_bit(DDRD,  PD2);
    set_bit(PORTD, PD2);

    // clk_enc config === INPUT_PULLUP
    clr_bit(DDRD,  PD3);
    set_bit(PORTD, PD3);

    // data_enc config === INPUT_PULLUP
    pinMode(data_enc, INPUT_PULLUP);

    // Config interrupts
}

void Eixo::resetPosition(void)
{

}

void Eixo::encInterrupt(void)
{

}

void endOfCourse(void)
{

}
