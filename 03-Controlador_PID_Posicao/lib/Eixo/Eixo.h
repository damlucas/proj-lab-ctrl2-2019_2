


#ifndef _Eixo_h_
#define _Eixo_h_

#endif

#include "stdint.h"
#include "Arduino.h"

class Eixo
{
    private:
        const uint8_t   end_of_course = 2,    // Sensor de fim de curso e clk do encoder
                        clk_enc = 3;
    public:
        uint8_t data_enc;               // Conexão do encoder
        bool increment_order = true;    // true defalt
        Eixo(uint8_t _data_enc, bool increment);
        Eixo(uint8_t _data_enc);
        void begin(void);
        void resetPosition(void);
        void encInterrupt(void);
        void endOfCourse(void);
};

