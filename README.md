# Obtenção da função de transferência de uma planta real via MatLab e projeto de um controlador PID

 Projeto submetido como requisito parcial para aprovação na disciplina de Laboratório de Controle 2 no período 2019.2 - UNIVASF.
 
O subprojeto ObterPlanta implementado para arduino uno, mas que pode ser facilmente escalável para outras plataformas, disponbiliza na porta serial o tempo de execução do experimento e os dados de entrada(PWM aplicado ao motor) e posição (calculada através do encoder) que serão passados para o MATLAB.
Este projeto prevê uma subrotina de calibração, responsável por zerar a posição do cabeçote.