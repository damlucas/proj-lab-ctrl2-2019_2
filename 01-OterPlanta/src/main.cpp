
/*=================================================================================================
*  Projeto:   FTPR (Função de Transferência de Planta Real)
*
*  Objetivo:  Obter FTPR para o caso de um cabeçote de impressão
*
*  Obs.:      
*
*  Autor:     Lucas Damião  - Estudante de Eng. Elétrica - UNIVASF
*
*  Criado:    22/04/19  Mod.: dd/mm/aa
*
*================================================================================================*/

/* --- Bibliotecas --- */
#include <Arduino.h>

/* --- Definições --- */
#define pFC 2               // Conexão do sensor de fim de curso

#define encA 3              // Conexão do encoder
#define encB 4

#define p1Motor 9           // Controle do motor
#define p2Motor 10

#define conversor 0.17        // Fator de conversão pulsos => mm
#define posMin    0.0					// Posição mínima do cabeçote em mm (ajustável)
#define posMax    288.0				// Posição máxima do cabeçote em mm (ajustável)

    // Testar o valor mínimo para mover o motor
#define minPWM 140           // Valores mínimo e máximo de PWM para aplicar ao motor
#define maxPWM 255

#define tc 200


/* --- Classes --- */

/* --- Declaração de Funções Adicionais --- */
void bordaA();                    // Função a ser chamada pela interrupção do encXa
void fimdeCurso();					      // Função a ser chamada pela interrupção do pino fcX
void zerarPos();						      // Função para zerar a posição do eixo

/* --- Objetos --- */

/* --- Variáveis Globais --- */
volatile int encoder = 0;					// Contador do sinal do encoder linear
float posicao = 0.0;
unsigned long ti;
float t;

/* --- Funções Obrigatórias --- */
void setup()
{
  Serial.begin(500000);
  // Aguarda uma comunicação USB Serial 
  while (!Serial);
  Serial.print(F("Configurando pinos de E/S..."));
  
  pinMode(pFC, INPUT_PULLUP);
  pinMode(encA, INPUT_PULLUP);
  pinMode(encB, INPUT_PULLUP);

  pinMode(p1Motor, OUTPUT);
  pinMode(p2Motor, OUTPUT);
  
  delay(tc);

  Serial.print(F(" Pinos configurados!\nConfigurando Interrupções..."));

  attachInterrupt(digitalPinToInterrupt(pFC), fimdeCurso, CHANGE);
  attachInterrupt(digitalPinToInterrupt(encA), bordaA, RISING);
  delay(tc);

  Serial.print(F(" Interrupções configuradas!\nInicializando sistemas adcionais..."));

  zerarPos();

  delay(tc);
  
  Serial.println(F(" Todos os sistemas inicializados!"));
  Serial.println(F("Precione qualquer tecla para iniciar..."));
  while (!Serial.available());
  
  ti = micros();
  
}

void loop()
{
  posicao = conversor*encoder;    // Atualiza a posição do cabeçote
  t = (micros()-ti)/1000000.0;
 
  
  if(t < 0.5 && posicao < 258.0)
    analogWrite(p1Motor,minPWM+40);
  else if(posicao > 260.0)
  {
    digitalWrite(p1Motor,0);
    digitalWrite(p2Motor,0);
    Serial.print(posicao);
    Serial.print("\t\t");
    Serial.print(minPWM+40);
    Serial.print("\t\t");
    Serial.println(encoder);
    while(1){}
  }
  Serial.print(t,8);
  Serial.print("\t");
  Serial.println(posicao,2);
}

/* --- Escopo das Funções Adicionais --- */
void bordaA()           // Função chamada pela interrupção em 'encA'
{
  if (!digitalRead(encB))
    encoder++;
  else
    encoder--;
}

void fimdeCurso()       // Função a ser chamada pela interrupção do pino 'fcX'
{
  digitalWrite(p1Motor,LOW);
  digitalWrite(p2Motor,LOW);
  encoder = 0;
}

void zerarPos()
{
  digitalWrite(p2Motor,LOW);
  analogWrite(p1Motor, minPWM);			// Move o cabeçote nos dois sentidos para garantir o 
                                    //   acionamento do sensor de fim de curso
  delay(2*tc);

  digitalWrite(p1Motor,LOW);
  analogWrite(p2Motor, minPWM);


  while (digitalRead(pFC) == HIGH) {}		// Aguarda o acionamento do sensor de fim de curso

  digitalWrite(p1Motor,HIGH);
  digitalWrite(p2Motor,HIGH);
  delay(2*tc);

  digitalWrite(p1Motor,LOW);
  digitalWrite(p2Motor,LOW);


  encoder = 0;
}
