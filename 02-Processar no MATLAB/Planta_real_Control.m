% Projeto:  Oter Planta
% Obs:      Usado Arduino Uno para gerar o sinal de entrada da planta
%           e fazer a leitura da posi��o do cabe�ote.
% Autor:    Lucas Dami�o
% Disc.:    Laborat�rio de Controle 2

clear; clc; close all;
%%
% Importa os dados de entrada e sa�da da planta
arq = 'Dados.xlsx';

t   = xlsread(arq, 'A2:A1122');
pwm = xlsread(arq, 'C2:C1122');
pos = xlsread(arq, 'B2:B1122');

Dados = iddata(pos,pwm,.0013);
%%
% Gr�ficos da entrada e da sa�da
figure(1)
yyaxis left;    plot(t, pwm);   grid
title('Sinais de entrada e sa�da da planta')
xlabel('Tempo (s)')
ylabel('PWM (normalizado)')
axis([0 2 0 1.2]);

yyaxis right;   plot(t, pos);
ylabel('Posi��o (mm)')
legend('Entrada','Sa�da');

%%

% Especifica o n�mero de polos e zeros que se deseja para a planta
np = 2;
nz = 1;

% Configura as op��es de estimativas
% Transfer function estimation
opts = tfestOptions;
opts.Display = 'on';
opts.WeightingFilter = [];

% A fun��o tfest retorna a estimativa da fun��o de transfer�cia
Ps = tfest(Dados, np, nz, opts);

%%

% Ps =
%  
%   From input "u1" to output "y1":
%      -1.047 s + 1648
%   ---------------------
%   s^2 + 7.191 s + 2.071
%  
% Continuous-time identified transfer function.
% 
% Parameterization:
%    Number of poles: 2   Number of zeros: 1
%    Number of free coefficients: 4
%    Use "tfdata", "getpvec", "getcov" for parameters and their uncertainties.
% 
% Status:                                           
% Estimated using TFEST on time domain data "Dados".
% Fit to estimation data: 98.94%                    
% FPE: 0.7206, MSE: 0.7129 

%==================================================

% Com a ferramenta rltool(), desenha o lugar das ra�zes da planta
%com a possibilidade de adicionar polos e zeros para o controlador

%rltool(Ps);

% C =
%  
%   -0.74172 (s-170) (s+5)
%   ----------------------
%         s (s+4000)
%  
% Name: C
% Continuous-time zero/pole/gain model.

%==================================================
 
% Controlador via PID Tuner (Type: PIDF)
%Response Time          0.1384
%Transient Behavior     0.37

%pidTuner(Ps)

% C =
%  
%              1            s    
%   Kp + Ki * --- + Kd * --------
%              s          Tf*s+1 
% 
%   with Kp = 0.123, Ki = 0.287, Kd = 0.00965, Tf = 0.0245
%  
% Continuous-time PIDF controller in parallel form.


